import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { ListComponent } from './list/list.component';
import { ListItemComponent } from './list-item/list-item.component';
import { WishlistItemComponent } from './wishlist-item/wishlist-item.component';

const components = [
  DashboardComponent,
  ListComponent,
  ListItemComponent,
  WishlistItemComponent
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DashboardRoutingModule,
    SharedModule,
  ],
  declarations: [
    ...components,
  ]
})
export class DashboardModule { }

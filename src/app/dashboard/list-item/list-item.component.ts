import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';

import { FoursquareService } from '../../shared/services';
import { getJwtToken } from '../../shared/helpers';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {
  @Output() public onAddToWishlist = new EventEmitter<boolean>();
  @Input() public checkin: any;

  public loggedUser: any;
  public user: any;
  public venue: any;
  public venuePhoto: any;

  constructor(
    private foursquareService: FoursquareService,
    private localStorageService: LocalStorageService
  ) { }

  public ngOnInit(): void {
    const accessToken: any = this.localStorageService.get('fsqAccessToken');

    this.loggedUser = getJwtToken();
    this.user = this.checkin.user;
    this.venue = this.checkin.venue;

    this.foursquareService.fetchVenuePhoto(this.venue.id)
      .subscribe((data: any) => {
        const photo = data.response.photos.items;
        if (photo instanceof Array && photo.length > 0) {
          this.venuePhoto = photo[0].prefix + '600x600' + photo[0].suffix;
        }
      });
  }

  public addToWishlist = (): void => {
    const confirmation = confirm('Are you sure?');
    if (confirmation) {
      this.foursquareService.registerWishlist(this.loggedUser.user_id, this.venue.id).subscribe(
        (data) => {
          this.onAddToWishlist.emit(true);
          alert('Has been added successfuly!');
        },
        (error) => {
          if (error && error.status === 422) {
            alert('has already been taken');
          } else {
            alert('Error on execute this action, try again later');
          }
        });
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';

import { FoursquareService } from '../../shared/services';
import { getJwtToken } from '../../shared/helpers';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  public checkins: any[] = [];
  public wishlist: any[] = [];
  public checkinsPreload = true;
  public wishlistPreload = true;

  constructor(
    private foursquareService: FoursquareService,
    private localStorageService: LocalStorageService,
  ) { }

  public ngOnInit(): void {
    this.fetchRecentCheckins();
    this.fetchWishlist();
  }

  /**
   * fetchRecentCheckins
   */
  public fetchRecentCheckins = (): void => {
    this.foursquareService.fetchRecentCheckins()
      .subscribe((data: any) => {
        this.checkins = data.response.recent || [];
        this.checkinsPreload = false;
      });
  }

  /**
   * fetchWishlist
   */
  public fetchWishlist = (): void => {
    this.wishlistPreload = true;
    const jwtData: any = getJwtToken();
    const params = {
      by_user: jwtData.user_id
    };
    this.foursquareService.fetchWishlist(params)
      .subscribe((data: any) => {
        const wishl: any[] = [];
        if (data instanceof Array && data.length > 0) {
          for (const item of data) {
            this.foursquareService.fetchVenue(item.venue.fsq_venue_id).subscribe((venue) => {
              const obj = Object.assign(
                venue.response.venue,
                { venue_api: item }
              );
              wishl.push(obj);
            });
          }
        }
        this.wishlist = wishl;
        this.wishlistPreload = false;
      });
  }

  /**
   * onAddToWishlist
   */
  public onAddToWishlist(event: boolean) {
    if (event) {
      this.fetchWishlist();
    }
  }

  /**
   * onRemoveWishlist
   */
  public onRemoveWishlist(event: boolean) {
    if (event) {
      this.fetchWishlist();
    }
  }

}

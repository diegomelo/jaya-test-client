import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { FoursquareService } from '../../shared/services';
import { getJwtToken } from '../../shared/helpers';

@Component({
  selector: 'app-wishlist-item',
  templateUrl: './wishlist-item.component.html',
  styleUrls: ['./wishlist-item.component.scss']
})
export class WishlistItemComponent implements OnInit {
  @Output() public onRemoveWishlist = new EventEmitter<boolean>();
  @Input() public venue: any;
  @Input() public venueId: any;

  public loggedUser = getJwtToken();
  public venuePhoto: any;

  constructor(
    private foursquareService: FoursquareService
  ) {}

  public ngOnInit(): void {
    if (this.venue.photos.groups instanceof Array && this.venue.photos.groups.length > 0) {
      const photoGroupItems: any = this.venue.photos.groups;
      if (
        photoGroupItems instanceof Array &&
        photoGroupItems[0].items.length > 0
      ) {
        this.venuePhoto = photoGroupItems[0].items[0].prefix + '600x600' + photoGroupItems[0].items[0].suffix;
      }
    }
  }

  public remove = (): void => {
    const venueId = this.venue.venue_api.id;
    const confirmation = confirm('Are you sure?');
    if (confirmation) {
      this.foursquareService.removeWishlist(venueId)
        .subscribe((data) => {
          this.onRemoveWishlist.emit(true);
          alert('Has been deleted successfully.');
        });
    }
  }

}

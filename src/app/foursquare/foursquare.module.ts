import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { SharedModule } from '../shared/shared.module';
import { FoursquareRoutingModule } from './foursquare-routing.module';
import { CallbackComponent } from './callback/callback.component';

@NgModule({
  imports: [
    HttpClientModule,
    CommonModule,
    SharedModule,
    FoursquareRoutingModule,
  ],
  declarations: [CallbackComponent]
})
export class FoursquareModule { }

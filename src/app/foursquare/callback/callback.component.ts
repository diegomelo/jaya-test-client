import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';

import { FoursquareService } from '../../shared/services';

@Component({
  selector: 'app-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.scss']
})
export class CallbackComponent implements OnInit {

  constructor(
    private foursquareService: FoursquareService,
    private activatedRoute: ActivatedRoute,
    private localStorageService: LocalStorageService,
    private router: Router,
  ) { }

  public ngOnInit(): void {
    this.fetchToken();
  }

  private fetchToken = (): void => {
    const code = this.activatedRoute.snapshot.queryParams['code'] || '';
    this.localStorageService.clearAll();

    this.foursquareService.auth(code)
      .subscribe((data) => {
        const userData = data;
        if (!userData.error) {
          this.localStorageService.set('jwt', userData.jwt);
          this.localStorageService.set('fsqAccessToken', userData.access_token);
          this.router.navigate(['dashboard']);
        } else {
          alert('Error! Session has been expired, try again later.');
          this.router.navigate(['']);
        }
      });
  }

}

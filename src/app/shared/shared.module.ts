import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { JwtHelper } from 'angular2-jwt';

import { FoursquareService } from './services';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { TopBannerComponent } from './components/top-banner/top-banner.component';
import { FooterComponent } from './components/footer/footer.component';

const components = [
  TopBarComponent,
  TopBannerComponent,
  FooterComponent
];

const providers = [
  FoursquareService,
  JwtHelper
];

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  declarations: [
    ...components
  ],
  exports: [
    ...components
  ],
  providers: [
    providers
  ]
})
export class SharedModule { }

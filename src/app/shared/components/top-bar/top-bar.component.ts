import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
import { JwtHelper } from 'angular2-jwt';

import { JwtUser } from '../../interfaces';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {
  public userData: JwtUser;

  constructor(
    private localStorageService: LocalStorageService,
    private jwtHelper: JwtHelper,
    private router: Router,
  ) { }

  public ngOnInit(): void {
    const jwtUser: any = this.localStorageService.get('jwt');
    if (jwtUser) {
      this.userData = this.jwtHelper.decodeToken(jwtUser);
    }
  }

  public logout = (): void => {
    this.localStorageService.clearAll();
    this.router.navigate(['']);
  }

}

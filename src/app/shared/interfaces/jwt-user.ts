export interface JwtUser {
  exp?: number;
  first_name?: string;
  last_name?: string;
  photo?: {
    prefix?: string,
    suffix?: string
  };
  user_id?: string | number;
}

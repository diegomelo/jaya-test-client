export interface FoursquareConf {
  client_id?: string;
  redirect_uri?: string;
}

import {
  isArray,
  isEmpty,
  isNil,
  isObjectLike,
  isPlainObject,
  merge
} from 'lodash';
import { LocalStorageService } from 'angular-2-local-storage';
import { JwtHelper } from 'angular2-jwt';

import { FoursquareConf } from '../interfaces';

const localStorage = new LocalStorageService({
  prefix: 'jaya-app',
  storageType: 'sessionStorage'
});

export const foursquareConf: FoursquareConf = {
  client_id: 'TFUOZTMDNWTSMK2H0SJE4KFTWTIMDZ44GVWP0RMP4WZZHE11',
  redirect_uri: 'http://localhost:4200/foursquare/callback'
};

export const apiHost = 'http://localhost:3000/';

export const foursqApiHost = 'https://api.foursquare.com/v2/';

export const getJwtToken = (): any => {
  const jwt: any = localStorage.get('jwt') || '';
  return jwt ? JwtHelper.prototype.decodeToken(jwt) : '';
};

export const getAccessToken = (): any => {
  return localStorage.get('fsqAccessToken') || '';
};

import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { LocalStorageService } from 'angular-2-local-storage';

import { apiHost, foursqApiHost, getAccessToken } from '../helpers';

@Injectable()
export class FoursquareService {
  private version = '20170813';
  private headers = {
    headers: new HttpHeaders().set(
      'Authorization',
      'Bearer ' + this.localStorageService.get('jwt') || ''
    ),
  };
  private accessToken = getAccessToken();

  constructor(
    private httpClient: HttpClient,
    private localStorageService: LocalStorageService
  ) {}

  public auth = (code: string): any => {
    const authEndPoint = apiHost + 'v1/foursquare/auth';
    const params = {
      code: code
    };
    return this.httpClient.post(authEndPoint, params).catch(this.handleError);
  }

  public registerWishlist = (user_id: number, venue_id: string): any => {
    const authEndPoint = apiHost + 'v1/wishlists';
    const params = {
      wishlist: {
        user_id: user_id,
        venue_id: venue_id
      }
    };
    return this.httpClient.post(authEndPoint, params, this.headers).catch(this.handleError);
  }

  public fetchWishlist = (obj?: any): any => {
    const params = obj || {};
    const authEndPoint = apiHost + 'v1/wishlists' + this.objToParams(params);
    return this.httpClient.get(authEndPoint, this.headers).catch(this.handleError);
  }

  public removeWishlist = (id?: any): any => {
    const authEndPoint = apiHost + 'v1/wishlists/' + id;
    return this.httpClient.delete(authEndPoint, this.headers).catch(this.handleError);
  }

  public fetchUsers = (self: boolean, id?: number): any => {
    let endPointSlip = '';
    if (self) {
      endPointSlip = '/self';
    } else if (id) {
      endPointSlip = '/' + id;
    }
    const params = {
      oauth_token: this.accessToken,
      v: this.version
    };
    const authEndPoint = foursqApiHost + 'users' + endPointSlip + this.objToParams(params);

    return this.httpClient.get(authEndPoint).catch(this.handleError);
  }

  public fetchRecentCheckins = (): any => {
    const params = {
      oauth_token: this.accessToken,
      v: this.version,
      limit: 50
    };
    const authEndPoint = foursqApiHost + 'checkins/recent' + this.objToParams(params);

    return this.httpClient.get(authEndPoint).catch(this.handleError);
  }

  public fetchVenuePhoto = (venueId: number): any => {
    const params = {
      oauth_token: this.accessToken,
      v: this.version,
      offset: 0,
      limit: 1
    };
    const authEndPoint = foursqApiHost + 'venues/' + venueId + '/photos' + this.objToParams(params);

    return this.httpClient.get(authEndPoint).catch(this.handleError);
  }

  public fetchVenue = (venueId: number): any => {
    const params = {
      oauth_token: this.accessToken,
      v: this.version,
      offset: 0,
      limit: 1
    };
    const authEndPoint = foursqApiHost + 'venues/' + venueId + this.objToParams(params);

    return this.httpClient.get(authEndPoint).catch(this.handleError);
  }

  protected handleError = (err: HttpErrorResponse): Observable<any> => {
    return Observable.throw(err || new HttpErrorResponse({
      status: 500,
      statusText: 'Internal Server Error'
    }));
  }

  /**
   * Converts an object to a parametrised string.
   * @param object
   * @returns {string}
   */
  protected objToParams = (obj: object) => {
    let str = '';
    if (obj instanceof Object) {
      for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
          if (str !== '') {
            str += '&';
          }
          str += key + '=' + encodeURIComponent(obj[key]);
        }
      }
    }

    return '?' + str;
  }

}
